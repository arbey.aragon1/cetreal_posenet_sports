var socket = io.connect("https://arbey.localtunnel.me", { forceNew: true });
//"http://localhost:8080"

const video = document.querySelector("video");

const constraints = {
  video: {
    width: 426,
    height: 240
  }
};

// request access to webcam
navigator.mediaDevices.getUserMedia(constraints).then(function(stream) {
  video.srcObject = stream;
  stream.getTracks().forEach(function(track) {
    console.log(track.getSettings());
  });
});

// returns a frame encoded in base64
const getFrame = () => {
  const canvas = document.createElement("canvas");
  canvas.width = video.videoWidth;
  canvas.height = video.videoHeight;
  canvas.getContext("2d").drawImage(video, 0, 0);
  const data = canvas.toDataURL("image/png");
  return data;
};

function uuidv4() {
  return "xxxy".replace(/[xy]/g, function(c) {
    var r = (Math.random() * 16) | 0,
      v = c == "x" ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
}

const FPS = 1;
var startCapture = 0;
var intervalVar;
const uuid = uuidv4();
var movie = [];
var today;
var capture;

socket.on("start-record-clients", function(data) {
  if (startCapture === 0) {
    data.time;
    var d = new Date();
    var n = d.getTime();
    console.log("start-record-clients");
    setTimeout(() => {
      console.log("--Start--");

      intervalVar = setInterval(() => {
        console.log("Capture");
        today = new Date();
        capture = getFrame();
        document.getElementById("myImg").src = capture;
        movie.push({
          date: today.getTime(),
          data: capture
        });
      }, 1000 / FPS); /** */
      /*console.log("Capture");
      today = new Date();
      capture = getFrame();
      document.getElementById("myImg").src = capture;
      movie.push({
        date: today.getTime(),
        data: capture
      });
      /** */
    }, data.time - n);
    startCapture = 1;
  }
});

socket.on("stop-record-clients", function(data) {
  if (startCapture === 1) {
    var d = new Date();
    var n = d.getTime();
    console.log("stop-record-clients");
    setTimeout(() => {
      console.log("--Stop--");
      clearInterval(intervalVar);
      startCapture = 2;
    }, data.time - n);
  }
});

socket.on("send-record-clients", function(data) {
  if (startCapture === 2) {
    console.log("send-record-clients");
    socket.emit("send-records", { uuid: uuid, movie: movie });
    startCapture = 3;
  }
});

function StartRecord() {
  socket.emit("start-record-server", {});
}

function StopRecord() {
  socket.emit("stop-record-server", {});
}

function SendRecord() {
  socket.emit("send-record-server", {});
}

/*
setInterval(() => {
  const test = getFrame();
  console.log(test);
  document.getElementById("myImg").src = test;
  socket.emit("camera", {uuid: uuidv4() , date: d.getTime(), data: test });
}, 1000);/** */

var express = require("express");
var app = express();
var server = require("http").Server(app);
var io = require("socket.io")(server);
var fs = require("fs");
const multer = require("multer");
const upload = multer();

var messages = [
  {
    id: 1,
    text: "Hola soy un mensaje",
    author: "Carlos Azaustre"
  }
];

app.use(express.static("public"));

app.get("/hello", function(req, res) {
  res.status(200).send("Hello World!");
});

io.on("connection", function(socket) {
  console.log("Alguien se ha conectado con Sockets");

  socket.on("start-record-server", function(data) {
    var d = new Date();
    var n = d.getTime();
    n = n - (n % (3 * 1000)) + 6 * 1000; //a los 10 segundos
    io.sockets.emit("start-record-clients", { time: n });
  });

  socket.on("stop-record-server", function(data) {
    var d = new Date();
    var n = d.getTime();
    n = n - (n % (3 * 1000)) + 6 * 1000; //a los 10 segundos
    io.sockets.emit("stop-record-clients", { time: n });
  });

  socket.on("send-record-server", function(data) {
    io.sockets.emit("send-record-clients", {});
  });

  socket.on("send-records", function(data) {
    console.log("send-records", data.uuid);
    console.log("send-records", data.movie[0].date);

    data.movie.forEach(element => {
      var strData = element.data;
      var base64Data = strData.replace(/^data:image\/png;base64,/, "");

      fs.writeFile(
        "public/video/" + data.uuid + "_" + element.date + ".png",
        base64Data,
        "base64",
        function(err) {
          console.log(err);
        }
      );
    });
  });

  socket.on("camera", function(msg) {
    console.log("*-*-*-*-*-*");
    //console.log(data);

    var base64Data = msg.data.replace(/^data:image\/png;base64,/, "");

    fs.writeFile("out.png", base64Data, "base64", function(err) {
      console.log(err);
    });
  });
}); /** */

server.listen(3000, function() {
  console.log("Servidor corriendo en http://localhost:3000");
});
